/*
 * Copyright (C) 2020-2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * einkaufszettel is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Lomiri.Components 1.3

ListItem{
    id: root

    property bool isChecked: marked
    onIsCheckedChanged: checkBox.checked = marked

    readonly property int    itemQuantity:  quantity
    readonly property string itemDimension: dimension


    leadingActions: ListItemActions{
        actions: [
            Action{
                iconName: "delete"
                onTriggered: entries.remove(uid)
            }
        ]
    }

    Item{
        id: content
        width: root.width
        height: root.height

        Rectangle{
            anchors.fill: parent
            color: theme.palette.normal.positive
            opacity: 0.2
            visible: marked
        }

        Item{
            id: leftSlot
            anchors{
                top: parent.top
                left: parent.left
                bottom: parent.bottom
            }
            width: units.gu(7)
        }

        Item{
            id: rightSlot
            anchors{
                top: parent.top
                right: parent.right
                bottom: parent.bottom
            }
            width: units.gu(7)
        }

        MouseArea{
            id: mouseCheckBox
            anchors.fill: settings.swapCheckBoxPosition ? rightSlot : leftSlot
            onClicked: entries.toggleMarked(uid)

            CheckBox{
                id: checkBox
                anchors.centerIn: parent
                checked: marked
                onTriggered: entries.toggleMarked(uid)
            }

            Label{
                id: lbCategory
                anchors{
                    bottom: parent.bottom
                    left:   settings.swapCheckBoxPosition ? undefined    : parent.left
                    right:  settings.swapCheckBoxPosition ? parent.right : undefined
                }
                visible: layoutProperties.viewMixedCategories
                textSize: Label.Small
                text: category
            }
        }

        MouseArea{
            id: dragMouse
            anchors.fill: settings.swapCheckBoxPosition ? leftSlot : rightSlot
            drag.target: content

            readonly property int iconSize: units.gu(3)
            readonly property int backgroundPadding: units.gu(1)

            Rectangle{
                anchors.centerIn: parent
                width: dragMouse.iconSize + 2*dragMouse.backgroundPadding
                height: width
                radius: dragMouse.backgroundPadding
                color: theme.palette.normal.base
                opacity: 0.8
            }

            Icon{
                anchors.centerIn: parent
                height: dragMouse.iconSize
                name: "sort-listitem"
                color: theme.palette.normal.baseText
            }
        }




        MouseArea{
            id: mouseQuantity
            anchors{
                verticalCenter: parent.verticalCenter
                left: leftSlot.right
            }
            height: lbQuantity.height + units.gu(2)
            width:  layoutProperties.quantityWidth + units.gu(2)

            // forward events to QuantitySelect's MouseArea
            onClicked:       lbQuantity.mouse.clicked(mouse)
            onDoubleClicked: lbQuantity.mouse.doubleClicked(mouse)
            onPressAndHold:  lbQuantity.mouse.pressAndHold(mouse)

            Rectangle{
                anchors.fill: parent
                radius: units.gu(1)
                color: theme.palette.normal.base
                opacity: 0.2
            }

            QuantitySelect{
                id: lbQuantity
                anchors{
                    verticalCenter: parent.verticalCenter
                    right: parent.right
                    rightMargin: units.gu(1)
                }
                font.bold: true
                quantity:  root.itemQuantity
                dimensionIndex: dimensions.getIndexBySymbol(root.itemDimension)
                onWidthChanged: {
                    if (width > layoutProperties.quantityWidth)
                        layoutProperties.quantityWidth = width
                }
                onQuantityChanged: {
                    entries.updateQuantity(uid,quantity)
                }
                onDimensionIndexChanged: {
                    entries.updateDimension(uid,dimensions.unitsModel.get(dimensionIndex).symbol)
                }
                mouse.onClicked: quantity += 1
                mouse.onDoubleClicked: lbQuantity.open()
                mouse.onPressAndHold:  lbQuantity.open()
            }
        }

        Label{
            id: lbName
            anchors{
                left: mouseQuantity.right
                right: rightSlot.left
                verticalCenter: parent.verticalCenter
                margins: units.gu(1)
            }
            wrapMode: Label.Wrap
            text: name
        }


        property int dragItemIndex: index

        states: [
            State {
                when: content.Drag.active
                ParentChange {
                    target: content
                    parent: listView
                }
            },
            State {
                when: !content.Drag.active
                AnchorChanges {
                    target: content
                    anchors.horizontalCenter: content.parent.horizontalCenter
                    anchors.verticalCenter: content.parent.verticalCenter
                }
            }
        ]
        Drag.active: dragMouse.drag.active
        Drag.hotSpot.x: content.width / 2
        Drag.hotSpot.y: content.height / 2
    }

    DropArea{
        anchors.fill: parent
        onEntered: {
            if (drag.source.dragItemIndex > index){
                entries.swap(entries.filteredEntryModel.get(drag.source.dragItemIndex).uid,uid)
            } else if (drag.source.dragItemIndex < index){
                entries.swap(uid,entries.filteredEntryModel.get(drag.source.dragItemIndex).uid)
            }
        }
    }
}
