/*
 * Copyright (C) 2020-2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * einkaufszettel is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import MDTK.Manual 1.0 as MDTKManual

MDTKManual.ManualDialog {
    id: root
    // TRANSLATORS: Shopping Lists = the title of this app
    title: i18n.tr("Welcome to <i>Shopping Lists</i>!")
    introduction: i18n.tr("This app helps you to organise your shopping lists.")

    MDTKManual.ManualHeader{
        title: i18n.tr("Usage")
    }
    MDTKManual.ManualParagraph{
        text: i18n.tr("Just enter new items in the text field on the top of the list. "
                      +"The new item is automatically added to the currently selected category. "
                      +"When you've put an item into your cart, you can select it and "
                      +"remove all items that you bought by clicking '%1'.").arg(i18n.tr("delete selected"))
    }


    MDTKManual.ManualHeader{
        title: i18n.tr("Categories")
    }
    MDTKManual.ManualParagraph{
        text: i18n.tr("You can organise different items separately by defining costum categories in the settings. "
                      +"Each category has its own list, so that items can be managed clearly and arranged by topics. "
                      +"In addition to costum categories, there are always two default categories, that can't be edited:")
    }
    MDTKManual.ManualParagraph{
        text: i18n.tr("%1: In this list, the items from all categories are shown.").arg("<i>"+i18n.tr("all")+"</i>")
    }
    MDTKManual.ManualParagraph{
        text: i18n.tr("%1: In this list, all items that do not belong to any other category are shown. If you delete a category that still has items in it, they will also end up here.").arg("<i>"+i18n.tr("other")+"</i>")
    }

    MDTKManual.ManualHeader{
        title: i18n.tr("Suggestions")
    }
    MDTKManual.ManualParagraph{
        text: i18n.tr("Based on your previous entries, you can get suggestions when typing new entries, filtered by your input and sorted by the frequency, you used an entry before. "
                      +"This makes it more convenient to input items that you frequently buy. "
                      +"You can manage the history, that is used to generate suggestions in the settings and remove items from there. "
                      +"If you don't like the suggestions, you can also disable this feature entirely.")
    }

    MDTKManual.ManualHeader{
        title: i18n.tr("Units")
    }
    MDTKManual.ManualParagraph{
        text: i18n.tr("To manage the amount you need from a certain item, you can use units. "
                      +"By default, you always enter one 'piece of' the item. If you enter an item, that exists already in the list, the specified amount of the item will be added to the existing entry.")
    }
    MDTKManual.ManualParagraph{
        text: i18n.tr("When you click on the quantity label in the list item, the amount gets incremented by one. When you double click or press and hold the label, the quantity edit dialog opens up.")
    }

    MDTKManual.ManualHeader{
        title: i18n.tr("Appearance")
    }
    MDTKManual.ManualParagraph{
        text: i18n.tr("You can choose between a dark or a light appearance and select from 8 different color flavours. "
                     +"The default setting is the magenta flavoured dark mode. "
                     +"You can change the appearance in the settings if you prefer an other flavour.")
    }
    MDTKManual.ManualParagraph{
        text: i18n.tr("If you prefer to have the check boxes on the right side of the screen, you can also change this in the settings.")
    }


    MDTKManual.ManualHeader{
        title: i18n.tr("Feedback")
    }
    MDTKManual.ManualParagraph{
        text: i18n.tr("Do you have any feedback? Want to see the source code? Found a bug? Missing a feature?")
    }
    MDTKManual.ManualParagraph{
        // TRANSLATORS: %1 contains a hyperlink with the displayed text "GitLab"
        text: i18n.tr("Feel free to get in touch on %1!").arg("<a href=\"https://gitlab.com/matdahl/einkaufszettel\">GitLab</a>")
        onLinkActivated: Qt.openUrlExternally(link)
    }
}
