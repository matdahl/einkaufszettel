/*
 * Copyright (C) 2020-2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * einkaufszettel is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Lomiri.Components 1.3
import MDTK 1.0 as MDTK

import "../components/"

MDTK.ThemedPage {
    id: root
    title: i18n.tr("Shopping Lists") + " - " + i18n.tr("Categories")

    headerTrailingActionBar.actions: [
        Action{
            iconName: "select-none"
            visible: db_categories.hasChecked
            onTriggered: db_categories.deselectAll()
        }
    ]

    function insertCategory(){
        if (!db_categories.exists(inputCategory.text))
            db_categories.insertCategory(inputCategory.text)
        inputCategory.text = ""
    }

    Row{
        id: inputRow
        anchors.top: header.bottom
        padding: units.gu(2)
        spacing: units.gu(2)
        TextField{
            id: inputCategory
            width: root.width - btNewCategory.width - 2*inputRow.padding - inputRow.spacing
            placeholderText: i18n.tr("new category ...")
            onAccepted: root.insertCategory()
        }
        Button{
            id: btNewCategory
            color: theme.palette.normal.positive
            width: 1.6*height
            enabled: inputCategory.text !== ""
            Icon{
                anchors.centerIn: parent
                height: 0.7*parent.height
                width:  height
                name:   "add"
                color:  theme.palette.normal.positiveText
            }
            onClicked: root.insertCategory()
        }
    }
    LomiriListView{
        id: listView
        clip: true
        anchors{
            top:    inputRow.bottom
            bottom: root.bottom
            left:   root.left
            right:  root.right
        }
        currentIndex: -1
        model: db_categories.model
        delegate: ListItemCategory{}
    }
    Label{
        anchors.centerIn: parent
        text: "("+i18n.tr("No entries") +")"
        visible: listView.model.count===0
    }

    ClearListButtons{
        id: clearButtons
        hasItems: listView.model.count>0
        hasCheckedItems: db_categories.hasChecked
        hasDeletedItems: db_categories.hasDeletedCategories

        onRemoveAll:      db_categories.removeAll()
        onRemoveSelected: db_categories.removeSelected()
        onRemoveDeleted:  db_categories.deleteAllRemoved()
        onRestoreDeleted: db_categories.restoreDeleted()
    }
}
