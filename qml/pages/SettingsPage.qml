/*
 * Copyright (C) 2020-2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * einkaufszettel is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import MDTK 1.0 as MDTK
import MDTK.Settings 1.0

MDTK.ThemedFlickablePage {
    id: root
    title: i18n.tr("Shopping Lists") + " - " + i18n.tr("Settings")

    SettingsMenuItem{
        id: stManual
        text: i18n.tr("Show manual")
        iconName: "info"
        onClicked: manual.open()
    }

    SettingsMenuItem{
        id: stCategories
        text: i18n.tr("Edit categories")
        iconName: "edit"
        subpage: "file:qml/pages/SettingsCategoriesPage.qml"
    }
    SettingsMenuItem{
        id: stUnits
        text: i18n.tr("Edit units")
        iconName: "edit"
        subpage: "file:qml/pages/SettingsUnitsPage.qml"
    }

    SettingsCaption{title: i18n.tr("Suggestions")}
    SettingsMenuSwitch{
        id: stHistoryEnabled
        text: i18n.tr("Show suggestions")
        Component.onCompleted: checked = db_history.active
        onCheckedChanged: db_history.active = checked
    }
    SettingsMenuSwitch{
        id: stAcceptOnClicked
        text: i18n.tr("Insert suggestion on click")
        Component.onCompleted: checked = db_history.acceptOnClick
        onCheckedChanged: db_history.acceptOnClick = checked
    }
    SettingsMenuItem{
        id: stHistory
        text: i18n.tr("Edit history")
        iconName: "edit"
        subpage: "file:qml/pages/SettingsHistoryPage.qml"
    }

    SettingsCaption{title: i18n.tr("Appearance")}
    SettingsThemeSelector{}
    SettingsAccentColorSelector{}
}
