/*
 * Copyright (C) 2020-2024  Matthias Dahlmanns
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * einkaufszettel is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Lomiri.Components 1.3
import Lomiri.Components.Popups 1.3
import MDTK 1.0 as MDTK

import "../components/"

MDTK.ThemedPage {
    id: root
    title: i18n.tr("Shopping Lists") + " - " + i18n.tr("Units")
    headerTrailingActionBar.actions: [
        Action{
            iconName: "select-none"
            visible: db_categories.hasChecked
            onTriggered: db_categories.deselectAll()
        }
    ]

    Row{
        id: inputRow
        anchors.top: header.bottom
        padding: units.gu(2)
        spacing: units.gu(2)
        TextField{
            id: inputSymbol
            width: units.gu(8)
            placeholderText: i18n.tr("abbr.")
            hasClearButton: false
        }
        TextField{
            id: inputName
            width: root.width - btNewUnit.width - 2*inputRow.padding - 2*inputRow.spacing - inputSymbol.width
            placeholderText: i18n.tr("new unit ...")
            onAccepted: btNewUnit.clicked()
        }
        Button{
            id: btNewUnit
            color: theme.palette.normal.positive
            width: 1.6*height
            Icon{
                anchors.centerIn: parent
                height: 0.7*parent.height
                width:  height
                name:   "add"
                color: theme.palette.normal.positiveText
            }
            onClicked: {
                if (inputSymbol.text !== "" && inputName.text !== ""){
                    dimensions.add(inputSymbol.text,inputName.text)
                    inputName.text = ""
                    inputSymbol.text = ""
                }
            }
        }
    }

    Label{
        anchors.centerIn: parent
        text: "("+i18n.tr("No entries") +")"
        visible: listView.model.count===0
    }

    LomiriListView{
        id: listView
        clip: true
        anchors{
            top:    inputRow.bottom
            bottom: root.bottom
            left:   root.left
            right:  root.right
        }
        currentIndex: -1
        model: dimensions.unitsModel
        delegate: ListItemUnit{}
    }

    ClearListButtons{
        id: clearList
        hasCheckedItems: dimensions.hasMarkedUnits
        hasDeletedItems: dimensions.hasDeletedUnits
        hasItems:        listView.model.count > 1
        onRemoveAll:      dimensions.removeAll()
        onRemoveSelected: dimensions.removeSelected()
        onRemoveDeleted:  dimensions.removeDeleted()
        onRestoreDeleted: dimensions.restoreDeleted()
    }

    Component {
         id: dialog
         Dialog {
             id: dialogue
             title: i18n.tr("Reset units")
             text: i18n.tr("Are you sure that you want to reset to default units?")
             Button {
                 text: i18n.tr("cancel")
                 onClicked: PopupUtils.close(dialogue)
             }
             Button {
                 text: i18n.tr("reset units")
                 color: LomiriColors.orange
                 onClicked: {
                     dimensions.resetUnits()
                     PopupUtils.close(dialogue)
                 }
             }
         }
    }

}
