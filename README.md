# Einkaufszettel

A simple app for [**Ubuntu Touch**](https://ubuntu-touch.io) to manage your shopping lists.

It allows to create different lists where items to be shopped can easily be collected.

## Features

* individual categories by which the entries can be sorted
* Ubuntu Style UI with a dark and a light theme in 8 different color flavors


## Translations

Currently, the app is available in the following languages:

| Language   | Translators | Status |
| ---------- | ----------- | ------ |
| Dutch      | @vistaus    | ![](https://img.shields.io/badge/dynamic/json?url=https%3A%2F%2Fgitlab.com%2Fapi%2Fv4%2Fprojects%2F47409657%2Fjobs%2Fartifacts%2Fxenial%2Fraw%2Ftranslation-status.json%3Fjob%3Dcheck-translations&query=%24.nl.progress&label=xenial) ![](https://img.shields.io/badge/dynamic/json?url=https%3A%2F%2Fgitlab.com%2Fapi%2Fv4%2Fprojects%2F47409657%2Fjobs%2Fartifacts%2Ffocal%2Fraw%2Ftranslation-status.json%3Fjob%3Dcheck-translations&query=%24.nl.progress&label=focal)  |
| English    | @matdahl    | default language |
| French     | @Anne017    | ![](https://img.shields.io/badge/dynamic/json?url=https%3A%2F%2Fgitlab.com%2Fapi%2Fv4%2Fprojects%2F47409657%2Fjobs%2Fartifacts%2Fxenial%2Fraw%2Ftranslation-status.json%3Fjob%3Dcheck-translations&query=%24.fr.progress&label=xenial) ![](https://img.shields.io/badge/dynamic/json?url=https%3A%2F%2Fgitlab.com%2Fapi%2Fv4%2Fprojects%2F47409657%2Fjobs%2Fartifacts%2Ffocal%2Fraw%2Ftranslation-status.json%3Fjob%3Dcheck-translations&query=%24.fr.progress&label=focal)  |
| German     | @matdahl    | ![](https://img.shields.io/badge/dynamic/json?url=https%3A%2F%2Fgitlab.com%2Fapi%2Fv4%2Fprojects%2F47409657%2Fjobs%2Fartifacts%2Fxenial%2Fraw%2Ftranslation-status.json%3Fjob%3Dcheck-translations&query=%24.de.progress&label=xenial) ![](https://img.shields.io/badge/dynamic/json?url=https%3A%2F%2Fgitlab.com%2Fapi%2Fv4%2Fprojects%2F47409657%2Fjobs%2Fartifacts%2Ffocal%2Fraw%2Ftranslation-status.json%3Fjob%3Dcheck-translations&query=%24.de.progress&label=focal) |
| Portuguese | @tiagotome95 | ![](https://img.shields.io/badge/dynamic/json?url=https%3A%2F%2Fgitlab.com%2Fapi%2Fv4%2Fprojects%2F47409657%2Fjobs%2Fartifacts%2Fxenial%2Fraw%2Ftranslation-status.json%3Fjob%3Dcheck-translations&query=%24.pt.progress&label=xenial) ![](https://img.shields.io/badge/dynamic/json?url=https%3A%2F%2Fgitlab.com%2Fapi%2Fv4%2Fprojects%2F47409657%2Fjobs%2Fartifacts%2Ffocal%2Fraw%2Ftranslation-status.json%3Fjob%3Dcheck-translations&query=%24.pt.progress&label=focal) |


Additional translations are very welcome and can be created by loading the file
**po/time-series.matdahl.pot** with a translation tool like [Poedit](https://poedit.net/)
and adding the resulting .po file to the po directory of this repository.


## Installation

The app is available in the Open Store: https://open-store.io/app/einkaufszettel.matdahl

Otherwise, this app can also be built using [**clickable**](https://gitlab.com/clickable/clickable).
Once `clickable` is installed on your computer, simply clone this repository, open the root directory of it in the commandline and execute the command `clickable` with your Ubuntu Phone connected and with developer mode enabled. **Clickable** then installs and start the app on your phone.

## License

Copyright (C) 2020-2024  Matthias Dahlmanns

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License version 3, as published
by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranties of MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.
